package com.jamieswhiteshirt.mssh;

import com.jamieswhiteshirt.mssh.common.CommonProxy;
import com.jamieswhiteshirt.mssh.common.MSSHServer;
import com.jamieswhiteshirt.mssh.common.command.PositionCommand;
import com.jamieswhiteshirt.mssh.common.command.RenameCommand;
import com.jamieswhiteshirt.mssh.common.command.WorldCommand;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppedEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = MSSH.MODID, version = MSSH.VERSION)
public class MSSH
{
    @Instance(MSSH.MODID)
    public static MSSH instance;

    @SidedProxy(clientSide = "com.jamieswhiteshirt.mssh.client.ClientProxy", serverSide = "com.jamieswhiteshirt.mssh.server.ServerProxy")
    public static CommonProxy proxy;

    public static final String MODID = "MSSH";
    public static final String VERSION = "1.0";

    public static Logger logger;

    private MSSHServer server;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        Configuration configuration = new Configuration(event.getSuggestedConfigurationFile());
        boolean enabled = configuration.getBoolean("Enabled", "ssh", true, "Enable or disable the SSH server.");
        int port = configuration.getInt("Port", "ssh", 22, 0, 65535, "The port the embedded SSH server will run on.");
        String hostKeyAlgorithm = configuration.getString("Host key pair algorithm", "ssh", "RSA", "The algorithm used for the host key pair.");
        String hostKeyFile = configuration.getString("Host key pair file", "ssh", "mssh/hostkey.ser", "The file containing the host key pair.");
        String acceptedPublicKeysFolder = configuration.getString("Accepted public keys folder", "auth", "mssh/keys", "The folder containing accepted public keys.");
        configuration.save();

        logger = event.getModLog();

        if (enabled)
        {
            server = new MSSHServer(logger, port);
            server.setHostKey(hostKeyAlgorithm, hostKeyFile);
            server.setPublicKeyAuth(acceptedPublicKeysFolder);
        }
    }

    @EventHandler
    public void serverLoad(FMLServerStartingEvent event)
    {
        event.registerServerCommand(new PositionCommand());
        event.registerServerCommand(new WorldCommand());
        event.registerServerCommand(new RenameCommand());

        if (server != null)
        {
            server.start(event.getServer());
        }
    }

    @EventHandler
    public void serverStopped(FMLServerStoppedEvent event)
    {
        if (server != null)
        {
            server.close();
        }
    }
}
