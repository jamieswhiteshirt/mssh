package com.jamieswhiteshirt.mssh.common;

import jline.console.completer.Completer;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;

import java.util.List;

public class MinecraftCommandCompleter implements Completer
{
    private final MinecraftServer server;
    private final ICommandSender sender;

    public MinecraftCommandCompleter(MinecraftServer server, ICommandSender sender)
    {
        this.server = server;
        this.sender = sender;
    }

    @Override
    public int complete(String buffer, int cursor, List<CharSequence> candidates)
    {
        List<String> list = server.getTabCompletions(sender, buffer, sender.getPosition());
        candidates.addAll(list);
        return findWordEdge(buffer, -1, cursor);
    }

    public int findWordEdge(String buffer, int step, int from)
    {
        int i = from;
        boolean flag = step < 0;
        int j = Math.abs(step);

        for (int k = 0; k < j; ++k)
        {
            if (!flag)
            {
                int l = buffer.length();
                i = buffer.indexOf(32, i);

                if (i == -1)
                {
                    i = l;
                }
            }
            else
            {
                while (i > 0 && buffer.charAt(i - 1) != 32)
                {
                    --i;
                }
            }
        }

        return i;
    }
}
