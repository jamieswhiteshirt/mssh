package com.jamieswhiteshirt.mssh.common;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

public interface IDynamicCommandSender extends ICommandSender
{
    /**
     * Set the name of this command sender
     */
    void setCommandSenderName(String name);

    /**
     * Set the position in the world
     */
    void setPos(BlockPos pos);

    /**
     * Set the world
     */
    void setWorld(World world);
}
