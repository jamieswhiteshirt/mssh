package com.jamieswhiteshirt.mssh.common;

import net.minecraft.server.MinecraftServer;
import org.apache.logging.log4j.Logger;
import org.apache.sshd.common.Factory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.auth.pubkey.PublickeyAuthenticator;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.session.ServerSession;

import java.io.IOException;
import java.nio.file.Paths;
import java.security.PublicKey;

public class MSSHServer
{
    private final SshServer sshd;
    private final Logger logger;

    public MSSHServer(Logger logger, int port)
    {
        this.sshd = SshServer.setUpDefaultServer();
        this.logger = logger;

        sshd.setPort(port);
    }

    public void setHostKey(String algorithm, String keyFile)
    {
        SimpleGeneratorHostKeyProvider hostKeyProvider = new SimpleGeneratorHostKeyProvider();
        hostKeyProvider.setAlgorithm(algorithm);
        hostKeyProvider.setPath(Paths.get(keyFile));
        hostKeyProvider.loadKeys();
        sshd.setKeyPairProvider(hostKeyProvider);
    }

    public void setPublicKeyAuth(String folder)
    {
        sshd.setPublickeyAuthenticator(new PublickeyAuthenticator() {
            @Override
            public boolean authenticate(String username, PublicKey key, ServerSession session) {
                return true;
            }
        });
    }

    public void start(final MinecraftServer server)
    {
        sshd.setShellFactory(new Factory<Command>() {
            @Override
            public Command create() {
                MSSHShell shell = new MSSHShell(server, logger);
                return shell;
            }
        });

        try {
            sshd.start();
            logger.info("Embedded SSH server started successfully.");
        }
        catch (IOException exception) {
            logger.error("Could not start embedded SSH server because of %s: %s", exception.getClass(), exception.getMessage());
        }
    }

    public void close()
    {
        try {
            sshd.close();
            logger.info("Embedded SSH server closed successfully.");
        }
        catch (IOException exception) {
            logger.error("Error while stopping embedded SSH server %s: %s", exception.getClass(), exception.getMessage());
        }
    }
}
