package com.jamieswhiteshirt.mssh.common;

import jline.UnixTerminal;
import jline.console.ConsoleReader;
import net.minecraft.command.CommandResultStats;
import net.minecraft.command.ICommandManager;
import net.minecraft.entity.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLLog;
import org.apache.logging.log4j.Logger;
import org.apache.sshd.common.util.threads.ThreadUtils;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.Environment;
import org.apache.sshd.server.ExitCallback;
import org.apache.sshd.server.SessionAware;
import org.apache.sshd.server.session.ServerSession;

import java.io.*;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

public class MSSHShell implements Command, SessionAware, Runnable, IDynamicCommandSender
{
    private InputStream in;
    private OutputStream out;
    private OutputStream err;
    private ExitCallback exitCallback;
    private ConsoleReader reader;
    private ServerSession session;

    private final Executor executor;
    private MinecraftServer server;
    private final Logger logger;

    private ICommandManager commandManager;
    private World world;
    private BlockPos pos;

    public MSSHShell(MinecraftServer server, Logger logger)
    {
        this.executor = ThreadUtils.newSingleThreadExecutor("shell");
        this.server = server;
        this.logger = logger;

        this.commandManager = server.getCommandManager();
        this.world = DimensionManager.getWorld(0);
        this.pos = new BlockPos(0, 0, 0);
    }

    @Override
    public void setInputStream(InputStream in)
    {
        this.in = in;
    }

    @Override
    public void setOutputStream(OutputStream out)
    {
        this.out = out;
    }

    @Override
    public void setErrorStream(OutputStream err)
    {
        this.err = err;
    }

    @Override
    public void setExitCallback(ExitCallback callback)
    {
        this.exitCallback = callback;
    }

    @Override
    public void setSession(ServerSession session)
    {
        this.session = session;
    }

    @Override
    public void start(Environment env) throws IOException
    {
        executor.execute(this);
    }

    @Override
    public void destroy()
    {
        if (executor instanceof ExecutorService)
        {
            ((ExecutorService)executor).shutdown();
        }
    }

    @Override
    public void run()
    {
        try {
            FMLLog.info("SSH connection established.");
            reader = new ConsoleReader(in, out, new UnixTerminal());
            reader.addCompleter(new MinecraftCommandCompleter(server, this));

            String line;
            while ((line = reader.readLine()) != null)
            {
                if ("exit".equals(line.trim()))
                {
                    break;
                }
                else
                {
                    commandManager.executeCommand(this, line);
                }
            }

            if (exitCallback != null) {
                exitCallback.onExit(0);
            }
            FMLLog.info("SSH connection closed.");
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
            if (exitCallback != null) {
                exitCallback.onExit(1, "IOException");
                FMLLog.info("SSH connection closed unexpectedly.");
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            if (exitCallback != null) {
                exitCallback.onExit(2, "Exception");
                FMLLog.info("SSH connection closed unexpectedly.");
            }
        }
    }

    @Override
    public String getCommandSenderName()
    {
        String username = session.getUsername();
        if (username == null || username.length() == 0)
        {
            username = "Unknown";
        }
        return username + "(SSH)";
    }

    @Override
    public IChatComponent getDisplayName()
    {
        return new ChatComponentText(getCommandSenderName());
    }

    @Override
    public void addChatMessage(IChatComponent component)
    {
        try
        {
            reader.println(component.getUnformattedText());
            reader.flush();
        }
        catch (IOException e)
        {
            FMLLog.info("Could not send chat message to SSH client");
        }
    }

    @Override
    public boolean canCommandSenderUseCommand(int permLevel, String commandName)
    {
        return true;
    }

    @Override
    public BlockPos getPosition()
    {
        return pos;
    }

    @Override
    public Vec3 getPositionVector()
    {
        return new Vec3(pos.getX(), pos.getY(), pos.getZ());
    }

    @Override
    public World getEntityWorld()
    {
        return world;
    }

    @Override
    public Entity getCommandSenderEntity()
    {
        return null;
    }

    @Override
    public boolean sendCommandFeedback()
    {
        return true;
    }

    @Override
    public void setCommandStat(CommandResultStats.Type type, int amount)
    {}

    @Override
    public void setCommandSenderName(String name)
    {
        session.setUsername(name);
    }

    @Override
    public void setPos(BlockPos pos)
    {
        this.pos = pos;
    }

    @Override
    public void setWorld(World world)
    {
        this.world = world;
    }
}
