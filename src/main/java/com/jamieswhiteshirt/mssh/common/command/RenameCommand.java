package com.jamieswhiteshirt.mssh.common.command;

import com.jamieswhiteshirt.mssh.common.IDynamicCommandSender;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;

public class RenameCommand extends DynamicCommandBase
{
    @Override
    public void processCommand(IDynamicCommandSender sender, String[] args) throws CommandException
    {
        if (args.length == 1)
        {
            sender.setCommandSenderName(args[0]);
        }
        else
        {
            throw new WrongUsageException("commands.rename.usage");
        }
    }

    @Override
    public String getCommandName() {
        return "rename";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "commands.rename.usage";
    }
}
