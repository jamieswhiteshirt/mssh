package com.jamieswhiteshirt.mssh.common.command;

import com.jamieswhiteshirt.mssh.common.IDynamicCommandSender;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;

public abstract class DynamicCommandBase extends CommandBase
{
    public abstract void processCommand(IDynamicCommandSender sender, String[] args) throws CommandException;

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException
    {
        if (sender instanceof IDynamicCommandSender)
        {
            processCommand((IDynamicCommandSender)sender, args);
        }
        else
        {
            throw new WrongUsageException("commands.dynamic.invalidSender");
        }
    }
}
