package com.jamieswhiteshirt.mssh.common.command;

import com.jamieswhiteshirt.mssh.common.IDynamicCommandSender;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;

public class PositionCommand extends DynamicCommandBase
{
    @Override
    public void processCommand(IDynamicCommandSender sender, String[] args) throws CommandException
    {
        if (args.length == 3)
        {
            BlockPos blockPos = parseBlockPos(sender, args, 0, false);
            sender.setPos(blockPos);
        }
        else if (args.length == 0)
        {
            BlockPos blockPos = sender.getPosition();
            sender.addChatMessage(new ChatComponentText(blockPos.getX() + " " + blockPos.getY() + " " + blockPos.getZ()));
        }
        else
        {
            throw new WrongUsageException("commands.position.usage");
        }
    }

    @Override
    public String getCommandName() {
        return "position";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "commands.position.usage";
    }
}
