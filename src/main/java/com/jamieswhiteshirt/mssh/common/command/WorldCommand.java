package com.jamieswhiteshirt.mssh.common.command;

import com.jamieswhiteshirt.mssh.common.IDynamicCommandSender;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;

public class WorldCommand extends DynamicCommandBase
{
    @Override
    public void processCommand(IDynamicCommandSender sender, String[] args) throws CommandException
    {
        if (args.length == 1)
        {
            int worldID = parseInt(args[0]);
            World world = DimensionManager.getWorld(worldID);
            if (world != null)
            {
                sender.setWorld(world);
            }
            else
            {
                throw new CommandException("commands.world.missing");
            }
        }
        else if (args.length == 0)
        {
            int worldID = sender.getEntityWorld().provider.getDimensionId();
            sender.addChatMessage(new ChatComponentText(Integer.toString(worldID)));
        }
        else
        {
            throw new WrongUsageException("commands.world.usage");
        }
    }

    @Override
    public String getCommandName() {
        return "world";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "commands.world.usage";
    }
}
